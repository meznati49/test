package mez.test.partition;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.apache.commons.collections4.CollectionUtils;

public class Partitionner {

    private Partitionner() {
    }

    public static Optional<List<List<String>>> partition(List<String> originalList, int size){

        if (size <= 0 || CollectionUtils.isEmpty(originalList)) {
            return Optional.empty();
        }
        List<List<String>> result = new ArrayList<>();
        for (int i = 0; i < originalList.size(); i++) {
            if (i % size == 0) {
                result.add(new ArrayList<>());
            }
            result.get(result.size() - 1).add(originalList.get(i));

        }
        return Optional.of(result);
    }
}
