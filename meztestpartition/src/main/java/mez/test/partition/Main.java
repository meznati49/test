package mez.test.partition;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class Main {

    public static void main(String[] args) {

        List<String> listToPartition = Arrays.asList("aa", "bb", "cc", "dd", "ee", "ff", "ii", "jj", "kk", "ll", "mm", "nn", "oo", "pp");

        ExecutorService executorService = Executors.newFixedThreadPool(10);
        Future<Optional<List<List<String>>>> future = executorService.submit(() -> Partitionner.partition(listToPartition,3));
        Optional<List<List<String>>> result ;
        try {
            //reading comes here
            result = future.get();
            if (result.isPresent()){
                List<List<String>> finalList = result.get();
                finalList.stream().flatMap(strings -> strings.stream()).forEach(s -> System.out.println(s));
                //..
            }
        } catch(InterruptedException e) {
            //todo log or treat properly. only for test
            e.printStackTrace();
        } catch(ExecutionException e) {
            //todo log or treat properly. only for test
            e.printStackTrace();
        }

    }
}
