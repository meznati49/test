package mez.test.partition;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class PartitionnerTest {

    @Test
    public void partition_should_return_empty_with_empty_list() {
        List<String> al = new ArrayList<>();

        Optional<List<List<String>>> result;

        result = Partitionner.partition(al, 2);

        Assert.assertFalse(result.isPresent());
    }

    @Test
    public void partition_should_return_empty_with_wrong_size() {
        List<String> al = Arrays.asList("aa", "bb");

        Optional<List<List<String>>> result;

        result = Partitionner.partition(al, 0);

        Assert.assertFalse(result.isPresent());

        result = Partitionner.partition(al, -5);

        Assert.assertFalse(result.isPresent());
    }

    @Test
    public void partition_should_work_with_size_four() {
        List<String> al = Arrays.asList("aa", "bb", "cc", "dd", "ee", "ff", "ii", "jj", "kk", "ll", "mm", "nn", "oo", "pp");

        Optional<List<List<String>>> result;

        result = Partitionner.partition(al, 4);

        Assert.assertTrue(result.isPresent());
        Assert.assertEquals(result.get().size(), 4);
    }


    @Test
    public void partition_should_work_with_size_extended() {
        List<String> al = Arrays.asList("aa", "bb", "cc", "dd", "ee");

        Optional<List<List<String>>> result;

        result = Partitionner.partition(al, 2);

        Assert.assertTrue(result.isPresent());
        Assert.assertEquals(result.get().size(), 3);
        Assert.assertEquals(result.get().get(result.get().size()-1).size(), 1);
        Assert.assertEquals(result.get().get(result.get().size()-1).get(0), "ee");
    }

    @Test
    public void partition_should_work_with_size_lt_list_size() {
        List<String> al = Arrays.asList("aa", "bb", "cc");

        Optional<List<List<String>>> result;

        result = Partitionner.partition(al, 4);

        Assert.assertTrue(result.isPresent());
        Assert.assertEquals(result.get().size(), 1);
        Assert.assertEquals(result.get().get(result.get().size()-1).size(), 3);
        Assert.assertEquals(result.get().get(result.get().size()-1).get(0), "aa");
    }
}